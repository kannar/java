# Copyright 2009-2014 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require ant java

SUMMARY="LALR parser generator for Java"
DESCRIPTION="Java Based Constructor of Useful Parsers (CUP for short). CUP is a
system for generating LALR parsers from simple specifications. It serves the same
role as the widely used program YACC and in fact offers most of the features of
YACC. However, CUP is written in Java, uses specifications including embedded
Java code, and produces parsers which are implemented in Java."
HOMEPAGE="http://www2.cs.tum.edu/projects/cup"
DOWNLOADS="${HOMEPAGE}/releases/${PN/_/-}-src-${PV/0.11b_p/11b-}.tar.gz"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/manual.html [[ lang = en ]]"
UPSTREAM_CHANGELOG="${HOMEPAGE}/develop/changelog.txt [[ lang = en ]]"

LICENCES="CUP"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES=""

WORK=${WORKBASE}
ANT_BUILD_DIR=${WORK}/dist
ANT_BUNDLED_LIBSDIR=( dist )
ANT_SRC_PREPARE_PARAMS=( init )
ANT_SRC_COMPILE_PARAMS=( dist )
ANT_SRC_TEST_PARAMS=( test )

src_compile() {
    # We need to compile this with the pre-built jar first...
    ant_src_compile
    edo rm bin/java-cup-11.jar
    edo cp dist/java-cup-11b.jar bin/java-cup-11.jar
    _ant_run clean
    # ... and again with the one we just built ourselves.
    ant_src_compile
}

src_install() {
    dodir /usr/share/${PN}
    insinto /usr/share/${PN}
    doins "${ANT_BUILD_DIR}"/java-cup-*.jar
}

